<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>
  <body>
    <?php
      include 'connection.php';
    ?>
    <div class="container">
      <div class="row" style="margin-top:100px;">
        <div class="col-md-6 offset-md-3">
          <form action="action.php?query=update" method="post" enctype="multipart/form-data">
            <?php
              $nip = $_GET['nip'];
              $query = "SELECT * FROM tb_pegawai WHERE nip='$nip'";
              $result = mysqli_query($conn,$query);
              $hasil = mysqli_fetch_assoc($result);
             ?>
            <div class="form-group">
              <label for="exampleFormControlInput1">NIP : <?php echo $nip; ?></label>
              <input name="nip" type="number" class="form-control" id="exampleFormControlInput1" placeholder="NIP" value="<?php echo $nip; ?>" hidden>
            </div>
            <div class="form-group">
              <label for="exampleFormControlInput1">NAMA</label>
              <input name="nama" type="text" class="form-control" id="exampleFormControlInput1" placeholder="NAMA" value="<?php echo $hasil['nama']; ?>">
            </div>
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Alamat</label>
              <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $hasil['alamat']; ?></textarea>
            </div>
            <div class="form-group">
              <label for="exampleFormControlFile1">Example file input</label>
              <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
