<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>
  <body>
    <?php
      include 'connection.php';
    ?>
    <div class="container-fuid">
      <div class="row" style="margin-top:100px;">
        <div class="col-md-1">

        </div>
        <div class="col-md-5">
          <form action="action.php?query=insert" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="exampleFormControlInput1">NIP</label>
              <input name="nip" type="number" class="form-control" id="exampleFormControlInput1" placeholder="NIP" required>
            </div>
            <div class="form-group">
              <label for="exampleFormControlInput1">NAMA</label>
              <input name="nama" type="text" class="form-control" id="exampleFormControlInput1" placeholder="NAMA" required>
            </div>
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Alamat</label>
              <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
            </div>
              <div class="form-group">
                <label for="exampleFormControlFile1">Example file input</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="file" required>
              </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
        <div class="col-md-5">
          <div class="col-md-12">
            <form class="form-inline" action="index.php?action=cari" method="post">
              <div class="form-group mx-sm-3 mb-2">
                <label for="cari" class="sr-only">Cari Pegawai</label>
                <input type="text" name="carinip" class="form-control" id="cari" placeholder="Cari Pegawai">
              </div>
              <button type="submit" class="btn btn-primary mb-2">Cari</button>
            </form>
          </div>
          <?php
          if(isset($_GET['action'])){
            switch ($_GET['action']) {
              case 'cari':
                $nip = $_POST['carinip'];
                ?>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">NIP</th>
                      <th scope="col">NAMA</th>
                      <th scope="col">ALAMAT</th>
                      <th scope="col">FOTO</th>
                      <th scope="col">AKSI</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                      $querycari = "SELECT * FROM tb_pegawai WHERE nama LIKE '%$nip%'";
                      $resultcari = mysqli_query($conn,$querycari);
                      $nomor = 0;
                      while ($hasilcari = mysqli_fetch_assoc($resultcari)) {
                      $nomor++;
                      echo "
                        <tr>
                          <th scope='row'>".$nomor."</th>
                          <td>".$hasilcari['nip']."</td>
                          <td>".$hasilcari['nama']."</td>
                          <td>".$hasilcari['alamat']."</td>
                          <td><img src='file/".$hasilcari['foto']."'></td>
                          <td><a href='edit.php?nip=".$hasilcari['nip']."'><button class='btn btn-success'>EDIT</button></a>&nbsp;<a href='action.php?query=delete&id=".$hasilcari['nip']."'><button class='btn btn-danger'>DELETE</button></a></td>
                        </tr>
                        ";
                      }
                     ?>
                  </tbody>
                </table>
                <?php
                break;

              default:

                break;
            }
          }else {
            ?>
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">NIP</th>
                  <th scope="col">NAMA</th>
                  <th scope="col">ALAMAT</th>
                  <th scope="col">FOTO</th>
                  <th scope="col">AKSI</th>
                </tr>
              </thead>
              <tbody>

                <?php
                  $query = "SELECT * FROM tb_pegawai";
                  $result = mysqli_query($conn,$query);
                  $no = 0;
                  while ($hasil = mysqli_fetch_assoc($result)) {
                  $no++;
                  echo "
                    <tr>
                      <th scope='row'>".$no."</th>
                      <td>".$hasil['nip']."</td>
                      <td>".$hasil['nama']."</td>
                      <td>".$hasil['alamat']."</td>
                      <td><img src='file/".$hasil['foto']."'></td>
                      <td><a href='edit.php?nip=".$hasil['nip']."'><button class='btn btn-success'>EDIT</button></a>&nbsp;<a href='action.php?query=delete&id=".$hasil['nip']."'><button class='btn btn-danger'>DELETE</button></a></td>
                    </tr>
                    ";
                  }
                 ?>
              </tbody>
            </table>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </body>
</html>
